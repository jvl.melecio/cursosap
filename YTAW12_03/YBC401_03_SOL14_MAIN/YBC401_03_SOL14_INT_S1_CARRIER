*&---------------------------------------------------------------------
*&  Include           YBC401_INT_S1_CARRIER
*&---------------------------------------------------------------------

CLASS lcl_airplane DEFINITION.
  PUBLIC SECTION.

    METHODS:
      constructor
        IMPORTING
          iv_name      TYPE string
          iv_planetype TYPE saplane-planetype
        EXCEPTIONS
          wrong_planetype,
      " otro metodo
      display_attributes.

    CLASS-METHODS:
      display_n_o_airplanes,
      class_constructor.

    EVENTS:
      airplane_created.


  PROTECTED SECTION.
    CONSTANTS:
    c_pos_1 TYPE i VALUE 30.

  PRIVATE SECTION.

    TYPES:
      ty_planetypes TYPE STANDARD TABLE OF saplane
    WITH NON-UNIQUE KEY planetype.

    "instanciados
    DATA: mv_name      TYPE string,
          mv_planetype TYPE saplane-planetype,
          mv_weight    TYPE saplane-weight,
          mv_tankcap   TYPE saplane-tankcap.
    "Estaticos
    CLASS-DATA:
      gv_n_o_airplanes TYPE i,
      gt_plane_types   TYPE ty_planetypes.

    CLASS-METHODS:
      get_technical_attributes
        IMPORTING
          iv_type    TYPE saplane-planetype
        EXPORTING
          ev_weight  TYPE saplane-weight
          ev_tankcap TYPE saplane-tankcap
        EXCEPTIONS
          wrong_planetype.
ENDCLASS.

CLASS lcl_airplane IMPLEMENTATION.

  METHOD class_constructor.
    SELECT * FROM saplane INTO TABLE gt_plane_types.
  ENDMETHOD.

  METHOD constructor.

    mv_name = iv_name.
    mv_planetype = iv_planetype.

    get_technical_attributes(
    EXPORTING iv_type = iv_planetype
    IMPORTING ev_weight = mv_weight
              ev_tankcap = mv_tankcap
     EXCEPTIONS wrong_planetype = 1  ).


    IF sy-subrc <> 0.
      RAISE wrong_planetype.
    ELSE.
*        mv_weight = LS_PLANETYPE-WEIGHT.
*        mv_tankcap = LS_PLANETYPE-TANKCAP.
      gv_n_o_airplanes = gv_n_o_airplanes + 1.
      RAISE EVENT airplane_created.
    ENDIF.

  ENDMETHOD.

  METHOD display_attributes.
    " definicion de variables
    DATA: lv_weight TYPE saplane-weight,
          lv_cap    TYPE saplane-tankcap.

    "Imprimo en pantalla los datos de la instancia.
    WRITE: / icon_ws_plane AS ICON,
          / 'Name of airplane', AT c_pos_1 mv_name,
          / 'Type of airplane', AT c_pos_1 mv_planetype,
          / 'weight', AT c_pos_1 mv_weight LEFT-JUSTIFIED,
          / 'tank', AT c_pos_1 mv_tankcap LEFT-JUSTIFIED.
  ENDMETHOD.

  METHOD display_n_o_airplanes.
    SKIP.
    WRITE: /'Number of aiplanes', AT c_pos_1 gv_n_o_airplanes LEFT-JUSTIFIED.
  ENDMETHOD.

*  METHOD get_n_o_airplanes.
*    rv_count = gv_n_o_airplanes.
*  ENDMETHOD.

  METHOD get_technical_attributes.
    DATA: ls_planetype TYPE saplane.

    READ TABLE gt_plane_types INTO ls_planetype
      WITH TABLE KEY planetype = iv_type TRANSPORTING weight tankcap.

    IF sy-subrc = 0.
      ev_weight = ls_planetype-weight.
      ev_tankcap = ls_planetype-tankcap.
    ELSE.
      RAISE wrong_planetype.
    ENDIF.
  ENDMETHOD.
ENDCLASS.

CLASS lcl_cargo_plane DEFINITION INHERITING FROM lcl_airplane.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING  iv_name      TYPE string
                   iv_planetype TYPE saplane-planetype
                   iv_cargo     TYPE s_plan_car
        EXCEPTIONS
                   wrong_planetype,

      display_attributes REDEFINITION,
      get_cargo RETURNING VALUE(rv_cargo) TYPE s_plan_car.

  PRIVATE SECTION.
    DATA:
          mv_cargo TYPE s_plan_car.


ENDCLASS.

CLASS lcl_cargo_plane IMPLEMENTATION.
  METHOD constructor.
    super->constructor(
      EXPORTING
        iv_name = iv_name
        iv_planetype = iv_planetype
      EXCEPTIONS
        wrong_planetype = 1 ).

    IF sy-subrc <> 0.
      RAISE wrong_planetype.
    ENDIF.

    "Conctruyo por diferencia
    mv_cargo = iv_cargo.
  ENDMETHOD.


  METHOD display_attributes.
    "Metodo heredado
    super->display_attributes( ).

    "Programacion por diferencia
    WRITE: / 'Max cargo:', AT c_pos_1 mv_cargo LEFT-JUSTIFIED.

    ULINE.


  ENDMETHOD.

  METHOD get_cargo.
    rv_cargo = mv_cargo.
  ENDMETHOD.

ENDCLASS.


CLASS lcl_passenger_plane DEFINITION INHERITING FROM lcl_airplane.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING  iv_name      TYPE string
                   iv_planetype TYPE saplane-planetype
                   iv_seats     TYPE s_seatsmax
        EXCEPTIONS
                   wrong_planetype,

      display_attributes REDEFINITION.

  PRIVATE SECTION.
    DATA:
          mv_seats TYPE s_seatsmax.

ENDCLASS.

CLASS lcl_passenger_plane IMPLEMENTATION.
  METHOD constructor.
    super->constructor(
      EXPORTING
        iv_name = iv_name
        iv_planetype = iv_planetype
      EXCEPTIONS
        wrong_planetype = 1 ).

    IF sy-subrc <> 0.
      RAISE wrong_planetype.
    ENDIF.

    "Conctruyo por diferencia
    mv_seats = iv_seats.
  ENDMETHOD.


  METHOD display_attributes.
    "Metodo heredado
    super->display_attributes( ).

    "Programacion por diferencia
    WRITE: / 'Max seats:', AT c_pos_1 mv_seats LEFT-JUSTIFIED.

    ULINE.


  ENDMETHOD.
ENDCLASS.


CLASS lcl_carrier DEFINITION.

  PUBLIC SECTION.

    INTERFACES: lif_partner.

    METHODS:
      constructor IMPORTING iv_name TYPE string,
      display_attributes.

  PRIVATE SECTION.
    DATA:
      mv_name      TYPE string,
      mt_airplanes TYPE TABLE OF REF TO lcl_airplane.

    METHODS:
      on_airplane_created FOR EVENT airplane_created
         OF lcl_airplane
      importing sender,
      display_airplanes.
    " get_max_cargo RETURNING VALUE(rv_max_cargo) TYPE s_plan_car.
ENDCLASS.


CLASS lcl_carrier IMPLEMENTATION.

  METHOD constructor.
    mv_name = iv_name.
    SET HANDLER on_airplane_created FOR ALL INSTANCES.
  ENDMETHOD.

  METHOD lif_partner~display_partner.
    display_attributes( ).
  ENDMETHOD.

  METHOD on_airplane_created.
    APPEND sender TO mt_airplanes.
  ENDMETHOD.

  METHOD display_attributes.

*    DATA:
*          lv_max_cargo TYPE s_plan_car.

    SKIP 2.
    WRITE: icon_flight AS ICON, mv_name.
    ULINE.
    ULINE.
    display_airplanes( ).
*    lv_max_cargo = me->get_max_cargo( ).
*    WRITE: / 'Capacity of biggest cargo plane:'(max),
*              lv_max_cargo LEFT-JUSTIFIED.

  ENDMETHOD.

*  METHOD add_airplane.
*    APPEND io_plane TO mt_airplanes.
*  ENDMETHOD.

  METHOD display_airplanes.
    DATA: lo_plane TYPE REF TO lcl_airplane.

    LOOP AT mt_airplanes INTO lo_plane.
      lo_plane->display_attributes( ).
    ENDLOOP.
  ENDMETHOD.

*  METHOD get_max_cargo.
*    DATA:
*          lo_plane TYPE REF TO lcl_airplane,
*          lo_cargo TYPE REF TO lcl_cargo_plane.
*
*    LOOP AT mt_airplanes INTO lo_plane.
*      TRY.
*        lo_cargo ?= lo_plane.
*
*        IF rv_max_cargo < lo_cargo->get_cargo( ).
*          rv_max_cargo = lo_cargo->get_cargo( ).
*         ENDIF.
*
*       CATCH cx_sy_move_cast_error.
*        " plane is not a cargo plane - do nothing
*        ENDTRY.
*      ENDLOOP.
*   ENDMETHOD.
ENDCLASS.